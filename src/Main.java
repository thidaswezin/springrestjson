import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			ObjectMapper mapper = new ObjectMapper();
			Employee employee = mapper.readValue(new File("data/sample-full.json"),Employee.class);
			System.out.println("First Name : "+employee.getFirstName());
			System.out.println("Last Name : "+employee.getLastName());
			Address address = employee.getAddress();
			System.out.println("Street : "+address.getStreet());
			for(String lan : employee.getLanguages()) {
				System.out.println("Languages : "+lan);
			}
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}

}
